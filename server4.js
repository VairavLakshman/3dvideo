/*jslint undef:true*/
/*jslint nomen:true*/
/*jslint es5:true*/
var csv = require('fast-csv');
var http = require('http');
var fs = require('fs');
var engines = require('ejs');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var csvFiles = [];                     //Array to store the csv files
var totalFiles = 30;
var count = 0;
var frames = [];
var requestCount = 0;
//Function to get the file names
function parseFiles(count) {
    "use strict";
    if (count < totalFiles) {
        if (count < 10) {
            csvFiles.push('../BINData2/PointCloud_bin/frame000' + count + '_x.csv');
            csvFiles.push('../BINData2/PointCloud_bin/frame000' + count + '_y.csv');
            csvFiles.push('../BINData2/PointCloud_bin/frame000' + count + '_z.csv');
        } else if (count < 100) {
            csvFiles.push('../BINData2/PointCloud_bin/frame00' + count + '_x.csv');
            csvFiles.push('../BINData2/PointCloud_bin/frame00' + count + '_y.csv');
            csvFiles.push('../BINData2/PointCloud_bin/frame00' + count + '_z.csv');
        } else if (count < 1000) {
            csvFiles.push('../BINData2/PointCloud_bin/frame0' + count + '_x.csv');
            csvFiles.push('../BINData2/PointCloud_bin/frame0' + count + '_y.csv');
            csvFiles.push('../BINData2/PointCloud_bin/frame0' + count + '_z.csv');
        } else {
            csvFiles.push('../BINData2/PointCloud_bin/frame' + count + '_x.csv');
            csvFiles.push('../BINData2/PointCloud_bin/frame' + count + '_y.csv');
            csvFiles.push('../BINData2/PointCloud_bin/frame' + count + '_z.csv');
        }
    }
}


app.set('port', process.env.port || 8080);
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname);
app.engine('html', engines.renderFile);
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//Function to parse data from the csvfiles
function parseCsv(fileName) {
    "use strict";
    /*jslint undef:true*/
    var position = [], promise;
    // Promises to ensure that the files are reading synchronously
    promise = new Promise(function (resolve, reject) {
        csv
            .fromPath(fileName)
            .on("data", function (data) {
                position.push(data);     //Pushing the resultant data into array
            })
            .on("end", function () {
                resolve(position);
            });
    });
    return promise;
}

//Function to accept the request from the client and in turn will send resultant X,Y,Z co-ordinates array
app.get('/getPcd', function (req, res) {
    'use strict';
    /*jslint undef:true*/
    if(frames.length < totalFiles) {
        var promises = [], k;
        parseFiles(count);
        for (k = 0; k < 3; k += 1) {    // Loop to form the promises array
            promises.push(parseCsv(csvFiles[k]));
        }
        Promise.all(promises).then(function (results) {
    //        "use strict";
            var field = [], i, j, l;
            for (l = 0; l < 3; l += 3) {      //Loop to get data from 3D array and flattened into single array
                for (i = 0; i < 240; i += 1) {  //In the resultant array the co-ordinates are in X,Y,Z respectively
                    for (j = 0; j  < 320; j += 1) {
                        field.push(parseFloat(results[l][i][j]));
                        field.push(parseFloat(results[l + 1][i][j]));
                        field.push(parseFloat(results[l + 2][i][j]));
                    }
                }
            }
            frames.push(field);           //Sending the resultant array to the client
            res.json("loading");
        });
    } else {
        if(requestCount < totalFiles) {
            res.json(frames[requestCount]);
            requ++;
        } else {
            res.json(null);
        }
    }
});

//Function to create the server in the given port
http.createServer(app).listen(app.get('port'), function () {
    'use strict';
    /*jslint undef:true*/
    console.log("Express server listening on port" + app.get('port'));
});