//var ws = new WebSocket("ws://127.0.0.1:8080", "echo-protocol");
//ws.onopen = function(){
//  console.log('connected')
//  ws.send(JSON.stringify({'type':'init'}))
//};
//ws.onmessage = function(messageEvent) {
//  var data = JSON.parse(messageEvent.data);
//  console.log(data['type']);
//}
if ( !Detector.webgl ) {
  Detector.addGetWebGLMessage();
}

var container,
    camera,
    controls,
    scene,
    renderer,
    mesh = null,
    position,
    canvas,
    ctx,
    ampCanvas,
    ampCtx,
    img,
    removeMeshCache = [];

function httpGet(url) {
  var promise = new Promise(function(resolve, reject) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
          console.log(xmlhttp.response);
            resolve(xmlhttp.response);
//          resolve(JSON.parse(xmlhttp.responseText));
        }
    };
  });
  return promise;
}

//function renderFrame(frameCount) {
//  PCDLoader(frameData[frameCount]);
//  phaseLoader(phaseData[frameCount]);
//  ampLoader(ampData[frameCount]);
//  frameCount++;
//  if (frameCount<totalFrames) {
//    setTimeout(renderFrame, 25, frameCount);
//  }
//}

function phaseLoader(phaseFrame) {
//  console.log(phaseFrame);
  var imageData = ctx.createImageData(canvas.width, canvas.height);
  imageData.data.set(new Uint16Array(phaseFrame));
  ctx.putImageData(imageData, 0, 0);
}

function ampLoader(ampFrame) {
  var imageData = ampCtx.createImageData(ampCanvas.width, ampCanvas.height);
  imageData.data.set(new Uint16Array(ampFrame));
  ampCtx.putImageData(imageData, 0, 0);
}

function PCDLoader(fields) {
    position = new Float32Array(fields);
    var geometry = new THREE.BufferGeometry();
    geometry.addAttribute('position', new THREE.BufferAttribute(position, 3));
    geometry.computeBoundingSphere();
    var material = new THREE.PointsMaterial({size: 0.005});
    mesh = new THREE.Points(geometry, material);

    if (removeMeshCache) {
        for (j = 0; j < removeMeshCache.length; j++) {
            scene.remove(removeMeshCache[j]);
        }
        removeMeshCache = [];
    }
    scene.add(mesh);
    removeMeshCache.push(mesh);
    renderer.render(scene, camera);
}

function init() {
    scene = new THREE.Scene();  //Adding scene
    var width = window.innerWidth;
    var height = window.innerHeight;
    var wrapper = document.getElementsByClassName('pcd-container')[0];
    camera = new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 0.01, 1000);
    camera.position.x = -0.05;
    camera.position.y = -0.25;
    camera.position.z = -1.75;
    camera.up.set(0, 0, 1);
    controls = new THREE.TrackballControls( camera );
    controls.rotateSpeed = 2.0;
    controls.zoomSpeed = 0.3;
    controls.panSpeed = 0.2;
    controls.noZoom = false;
    controls.noPan = false;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;
    controls.minDistance = 0.3;
    controls.maxDistance = 0.3 * 100;
    scene.add( camera );
    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor( 0x000000 );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    wrapper.appendChild( renderer.domElement );
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    ampCanvas = document.getElementById('ampcanvas');
    ampCtx = ampCanvas.getContext('2d');
}

//Function to rotate 3D mesh in real time
function animate() {
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
}

function afterrender() {
  init();
//  renderFrame(0);
  animate();
}

function toArrayBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint16Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return ab;
}

 httpGet("/getTotalFrames").then(function(totalFrames){
   var count = 0;
   function getFrame() {
     httpGet('/getFrame/'+count).then(function(frameData) {
//       console.log("PCD", frameData[0]);
//       console.log("phase", typeof(frameData[1]));
//       console.log("amp", typeof(frameData[2]));
       if (count < totalFrames) {
//         PCDLoader(frameData[0]);
//         phaseLoader(frameData[1]);
//         ampLoader(frameData[2]);
//         console.log("Frame Data", frameData);
//         var arrayBufAmp = toArrayBuffer(frameData);
//         console.log("Array Buffer", arrayBufAmp);
//         ampLoader(arrayBufAmp);
           ampLoader(frameData);
         count++;
         getFrame();
       }
     });
   }
   getFrame();
 });

// httpGet("/getTotalFrames").then(function(totalFrames){
//   var count = 0;
//   function getFrame() {
//     httpGet('/getFrame/'+count).then(function(frameData) {
//       if (count < totalFrames) {
//         data[count] = frameData;
//         // PCDLoader(frameData, count);
//         count++;
//         getFrame();
//       } else {
//         function renderFrame(frameCount) {
//           var frame = data[frameCount]
//           frameCount++
//           PCDLoader(frame)
//           if(frameCount <totalFrames) {
//               setTimeout(renderFrame, 20, frameCount)
//           } else {
//             renderFrame(0)
//           }
//         }
//         console.log('start Rendering.....')
//         renderFrame(0)
//       }
//     });
//   }
//   getFrame();
// });
