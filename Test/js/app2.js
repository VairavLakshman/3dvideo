var ffi = require('ffi');
var ref = require('ref');
var win = require('nw.gui').Window.get();
var arrayType = require('ref-array');
var totalFrames = 700;
var frameData = [];
var phaseData = [];
var ampData = [];
var RTLD_NOW = ffi.DynamicLibrary.FLAGS.RTLD_NOW,
  RTLD_GLOBAL = ffi.DynamicLibrary.FLAGS.RTLD_GLOBAL,
  mode = RTLD_NOW | RTLD_GLOBAL;
var floatArray = arrayType(ref.types.float);
var intArray = arrayType(ref.types.uint16);
var libVoxel = ffi.DynamicLibrary("C:/Users/Roshan/Desktop/voxelsdk-0.6.0/build/lib/voxel" + ffi.LIB_EXT, mode);
var libString = ffi.Library("C:/Users/Roshan/Desktop/voxelsdk-0.6.0/build/lib/Test", {'createArgv': [ 'int', [ 'int', 'string', 'string', 'string'] ], 'retrievePoints': ['int', ['int', floatArray, intArray, intArray] ]});
var name = 'someName';
var flag = '-f';
//  var path = 'C:/Users/Roshan/Desktop/webgl_input.vxl';
var path;
var container,
    camera,
    controls,
    scene,
    renderer,
    mesh = null,
    position,
    canvas,
    ctx,
    ampCanvas,
    ampCtx,
    img,
    removeMeshCache = [],
    maxProgress = 0,
    count = 0;

function clearMemory() { 
    for(var j=0;j<mesh.length;j++) {
        mesh[j].geometry.dispose();
        mesh[j].material.dispose();
        scene.remove(mesh[j]);
        mesh[j] = undefined;
        delete(mesh[j]);
    }
    mesh = undefined;
}

function loadingFile(frameCount) {
    var currentProgress = parseInt((frameCount / totalFrames) * 100);
    if (currentProgress > maxProgress) {
        document.getElementById("progress").innerHTML =  currentProgress + "%";
        maxProgress = currentProgress;
    }
    if(frameCount === 0) {    
        libString.createArgv(3, name, flag, path);
    }
    var sample = new floatArray(230400);
    var phaseSample = new intArray(307200);
    var ampSample = new intArray(307200);
    libString.retrievePoints(frameCount+1, sample, phaseSample, ampSample);
    ampData[frameCount] = ampSample;
    phaseData[frameCount] = phaseSample;
    var f32Array = new Float32Array(sample.buffer.buffer);
    frameData[frameCount] = f32Array;
    frameCount++;
    if (frameCount<totalFrames) {
        setTimeout(loadingFile, 10, frameCount);
    } else {
        currentProgress = 0;
        maxProgress = 0;
        document.getElementById("progress").innerHTML =  currentProgress + "%";
        afterrender();
    }
}

function afterrender() {
    document.getElementById("loading").style.visibility = "hidden";
    document.getElementById("progress").style.visibility = "hidden";
    if(count > 1) {
        document.getElementById("amplitude-container").style.visibility = "visible";
        document.getElementById("phase-container").style.visibility = "visible";
        document.getElementById("pcd-container").style.visibility = "visible";
        renderFrame(0);
    } else {
        init();
        animate();
        renderFrame(0);
    }
}

function renderFrame(frameCount) {
  PCDLoader(frameData[frameCount]);
  phaseLoader(phaseData[frameCount]);
  ampLoader(ampData[frameCount]);
  frameCount++;
  if (frameCount<totalFrames) {
    setTimeout(renderFrame, 25, frameCount);
  } else {
    document.getElementById("loading").style.visibility = "visible";
    document.getElementById("progress").style.visibility = "visible";
    document.getElementById("amplitude-container").style.visibility = "hidden";
    document.getElementById("phase-container").style.visibility = "hidden";
    document.getElementById("pcd-container").style.visibility = "hidden";
  }
}

function phaseLoader(phaseFrame) {
  var imageData = ctx.createImageData(canvas.width, canvas.height);
  imageData.data.set(new Uint16Array(phaseFrame.buffer.buffer));
  ctx.putImageData(imageData, 0, 0);
}

function ampLoader(ampFrame) {
  var imageData = ampCtx.createImageData(ampCanvas.width, ampCanvas.height);
  imageData.data.set(new Uint16Array(ampFrame.buffer.buffer));
  ampCtx.putImageData(imageData, 0, 0);
}


function PCDLoader(frame) {
    position = frame;
    var geometry = new THREE.BufferGeometry();
    geometry.addAttribute('position', new THREE.BufferAttribute(position, 3));
    geometry.computeBoundingSphere();
    var material = new THREE.PointsMaterial({size: 0.007});
    mesh = new THREE.Points(geometry, material);

    if (removeMeshCache) {
        for (j = 0; j < removeMeshCache.length; j++) {
            scene.remove(removeMeshCache[j]);
        }
        removeMeshCache = [];
    }
    scene.add(mesh);
    removeMeshCache.push(mesh);
    renderer.render(scene, camera);
}

function init() {
    scene = new THREE.Scene();  //Adding scene
    var width = window.innerWidth;
    var height = window.innerHeight;
    var wrapper = document.getElementById('pcd-container');
    camera = new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 0.01, 1000);
    camera.position.x = -0.05;
    camera.position.y = -0.25;
    camera.position.z = -1.75;
    camera.up.set(0, 0, 1);
    controls = new THREE.TrackballControls( camera );
    controls.rotateSpeed = 2.0;
    controls.zoomSpeed = 0.3;
    controls.panSpeed = 0.2;
    controls.noZoom = false;
    controls.noPan = false;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;
    controls.minDistance = 0.3;
    controls.maxDistance = 0.3 * 100;
    scene.add( camera );
    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor( 0x000000 );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( wrapper.clientWidth, wrapper.clientHeight );
    wrapper.appendChild( renderer.domElement );
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    ampCanvas = document.getElementById('ampcanvas');
    ampCtx = ampCanvas.getContext('2d');
}

//Function to rotate 3D mesh in real time
function animate() {
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
}


window.onload = function() {
    document.getElementById('getFile').onclick = function() {
        count++;
        document.getElementById('fileBrowse').click();
    };
    
    document.getElementById('clear').onclick = function() {
        clearMemory();
    }
    
    $('input[type=file]').change(function (e) {
        $('#customFileUpload').html($(this).val());
        path = $(this).val();
        console.log(path);
        loadingFile(0);
    });
}