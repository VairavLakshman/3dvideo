/*jslint undef:true*/
/*jslint nomen:true*/
/*jslint es5:true*/
var csv = require('fast-csv');
var http = require('http');
var fs = require('fs');
var engines = require('ejs');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();


app.set('port', process.env.port || 8080);
app.use(express.static(__dirname + '/'));
app.set('views', __dirname);
app.engine('html', engines.renderFile);
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


//App data
var totalFrames = 2;
var frames = {};


//Function to parse data from the csvfiles
function parseCsv(fileName) {
    "use strict";
    /*jslint undef:true*/
    var pcdData = [], promise;
    // Promises to ensure that the files are reading synchronously
    promise = new Promise(function (resolve, reject) {
        csv
            .fromPath(fileName)
            .on("data", function (data) {
                pcdData.push(data);     //Pushing the resultant data into array
            })
            .on("end", function () {
                resolve(pcdData);
            });
    });
    return promise;
}
app.get('/getFrame/:frame', function (req, res) {
    'use strict';
    /*jslint undef:true*/
    console.log(Object.keys(frames), req.params.frame)
    res.json(frames[2]);
});

app.get('/getTotalFrames', function (req, res) {
    'use strict';
    /*jslint undef:true*/
    res.json(totalFrames);
});

http.createServer(app).listen(app.get('port'), function () {
    // 'use strict';
    console.log('Starting')
    var coordinates, str, pad = "0000", iStr, promises = [];
    for (var i = 0; i < totalFrames; i += 1) {
        str = i.toString();
        iStr = pad.substring(0, pad.length - str.length) + str;
        for (coordinates of ['x', 'y', 'z']) {
            promises.push(parseCsv('../../BINData2/PointCloud_bin/frame' + iStr + '_' + coordinates + '.csv'))
        }
        Promise.all(promises).then(function(results){
            var field = [], k, j, l;
            for (l = 0; l < 3; l += 3) {      //Loop to get data from 3D array and flattened into single array
                for (k = 0; k < 240; k += 1) {  //In the resultant array the co-ordinates are in X,Y,Z respectively
                    for (j = 0; j  < 320; j += 1) {
                        field.push(parseFloat(results[l][k][j]));
                        field.push(parseFloat(results[l + 1][k][j]));
                        field.push(parseFloat(results[l + 2][k][j]));
                    }
                }
            }
            frames[i] = field;
            console.log(i, totalFrames)
            if(i===totalFrames-1) {
                /*jslint undef:true*/
                console.log("Express server listening on port" + app.get('port'));
            }
        })
    }
    console.log('Wait for server')
});
