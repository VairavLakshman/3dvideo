var ffi = require('ffi');
var ref = require('ref');
var arrayType = require('ref-array');
//var WebSocketServer = require('websocket').server;
var http = require('http');
var fs = require('fs');
var engines = require('ejs');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.set('port', process.env.port || 8080);
app.use(express.static(__dirname + '/'));
app.set('views', __dirname);
app.engine('html', engines.renderFile);
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//App data
var totalFrames = 30;
var container = [];
var frameData = [];
var phaseData = [];
var ampData = [];
var RTLD_NOW = ffi.DynamicLibrary.FLAGS.RTLD_NOW,
    RTLD_GLOBAL = ffi.DynamicLibrary.FLAGS.RTLD_GLOBAL,
    mode = RTLD_NOW | RTLD_GLOBAL;

var floatArray = arrayType(ref.types.float);
var intArray = arrayType(ref.types.uint16);
var libVoxel = ffi.DynamicLibrary("C:/Users/Roshan/Desktop/voxelsdk-0.6.0/build/lib/voxel" + ffi.LIB_EXT, mode);
var libString = ffi.Library("C:/Users/Roshan/Desktop/voxelsdk-0.6.0/build/lib/Test", {'createArgv': [ 'int', [ 'int', 'string', 'string', 'string'] ], 'retrievePoints': ['int', ['int', floatArray, intArray, intArray]]});
var name = 'someName';
var flag = '-f';
var path = 'C:/Users/Roshan/Desktop/webgl_input.vxl';
console.time('init');
libString.createArgv(3, name, flag, path);
console.timeEnd('init');
for(var i=0; i<=totalFrames; i++) {
  container.push([]);
}
for(var i=0; i<=totalFrames; i++) {
    var sample = new floatArray(230400);
    var phaseSample = new intArray(307200);
    var ampSample = new intArray(307200);
    libString.retrievePoints(i+1, sample, phaseSample, ampSample);
    ampData[i] = ampSample.buffer.buffer;
    phaseData[i] = phaseSample;
//    frameData[i] = sample;
//    var f32Array = new Float32Array();
//    frameData[i] = sample.buffer.buffer;
    frameData[i] = sample.buffer.buffer;
//    container[i].push(frameData[i])
//    container[i].push(phaseData[i].buffer.buffer)
////    container[i].push(phaseData[i])
//    container[i].push(ampData[i].buffer.buffer)
//    container[i].push(ampData[i])
}

//var ab2String = function(abuf)  {
//  return String.fromCharCode.apply(null, new Uint16Array(abuf));
//}

function toBuffer(ab) {
  var buf = new Buffer(ab.byteLength);
  var view = new Uint16Array(ab);
  for (var i = 0; i < buf.length; ++i) {
    buf[i] = view[i];
  }
  return buf;
}

app.get('/getFrame/:frame', function (req, res) {
//  console.log(container[req.params.frame][2]);
//  console.log(typeof(container[req.params.frame][0]));
//  var containerString = [];
//  containerString.push(ab2String(container[req.params.frame][0]));
//  containerString.push(ab2String(container[req.params.frame][1]));
//  containerString.push(ab2String(container[req.params.frame][2]));
//  console.log(containerString);
//   res.json(containerString);
//   res.json(container[req.params.frame]);
//  console.log(ampData[req.params.frame]);
  var bufferAmp = toBuffer(ampData[req.params.frame]);
  console.log(bufferAmp);
  res.end(bufferAmp, 'binary');
//  res.end(null, 'binary');
//  res.json(ampData[req.params.frame]);
});

app.get('/getTotalFrames', function (req, res) {
//  console.log()
   res.json(totalFrames);
});


var server = http.createServer(app);
server.listen(app.get('port'), function () {
  console.log("Server Listening to Port number : 8080");
    // var promises = [];
});
//wsServer = new WebSocketServer({
//  httpServer: server,
//  autoAcceptConnections: false
//});
//
//function originIsAllowed(origin) {
//  return true;
//}
//
//wsServer.on('request', function(request) {
//  if (!originIsAllowed(request.origin)) {
//    request.reject();
//    console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
//    return;
//  }
//
//  var connection = request.accept('echo-protocol', request.origin);
//  console.log((new Date()) + ' Connection accepted.');
//  connection.on('message', function(message) {
//    data = JSON.parse(message['utf8Data']);
//    if(data['type'] === 'init') {
////      console.log(totalFrames)
//      for(var i = 0; i<totalFrames; i++) {
//        connection.sendUTF(JSON.stringify({'type':'data', value: frameData[i]}));
//      }
//    }
//  });
//  connection.on('close', function(reasonCode, description) {
//    console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
//  });
//});
